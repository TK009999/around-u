//
//  CommonParameters.swift
//  Around-U
//
//  Created by haochanglee on 2015/5/9.
//  Copyright (c) 2015年 Ace.Lee. All rights reserved.
//

import UIKit
import ImageIO
import SwiftyJSON
import CoreLocation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


var myLatitude = CLLocationDegrees()
var myLongitude = CLLocationDegrees()

var mSetConstraint = SetConstraint()
var mQuery = Query()

var mFamilyMart: [String] = []
var mFamilyMartLat: [Double] = []
var mFamilyMartLng: [Double] = []

var mSevenEleven: [String] = []
var mSevenElevenLat: [Double] = []
var mSevenElevenLng: [Double] = []

var mHilife: [String] = []
var mHilifeLat: [Double] = []
var mHilifeLng: [Double] = []

var mOKMart: [String] = []
var mOKMartLat: [Double] = []
var mOKMartLng: [Double] = []

var mNewMarkCount = 0
var mNewMarkLat: [Double] = []
var mNewMarkLng: [Double] = []
var mNewMarkName = String()

var mDistance = "300"
class SetConstraint {
    
    var Constraints = [AnyObject]()
    
    func ConstraintTop(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat) -> NSLayoutConstraint {
        var Constraint = NSLayoutConstraint()
        Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.top, multiplier: 1.0, constant: constant)
        
        return Constraint
    }
    
    func ConstraintBottom(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat) -> NSLayoutConstraint {
        var Constraint = NSLayoutConstraint()
        Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.bottom, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: constant)
        
        return Constraint
    }
    
    func ConstraintTrailing(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat) -> NSLayoutConstraint {
        var Constraint = NSLayoutConstraint()
        Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.trailing, multiplier: 1.0, constant: constant)
        
        return Constraint
    }
    
    func ConstraintLeading(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat) -> NSLayoutConstraint {
        var Constraint = NSLayoutConstraint()
        Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: constant)
        
        return Constraint
    }
    
    func ConstraintWidth(_ item: AnyObject, constant: CGFloat) -> NSLayoutConstraint {
        var Constraint = NSLayoutConstraint()
        Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: constant)
        
        return Constraint
    }
    
    func ConstraintHeight(_ item: AnyObject, constant: CGFloat) -> NSLayoutConstraint {
        var Constraint = NSLayoutConstraint()
        Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: constant)
        
        return Constraint
    }
    
    func ConstraintCenterX(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat) -> NSLayoutConstraint {
        var Constraint = NSLayoutConstraint()
        Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: constant)
        
        return Constraint
    }
    
    func ConstraintCenterY(_ item: AnyObject, toItem: AnyObject?, constant: CGFloat) -> NSLayoutConstraint {
        var Constraint = NSLayoutConstraint()
        Constraint = NSLayoutConstraint(item: item, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: toItem, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: constant)
        
        return Constraint
    }

}

class Query {
    
    func googlePlaces(_ queryTyper: AnyObject) {
        let QUERY_GOOGLE_PLACES_API_KEY = "AIzaSyAzfYyiGajHCiEWYiXvCgruh9RF0nqhqc0"
        
        let SERVER_URL = URL(string: "https://maps.googleapis.com/maps/api/place/search/json?location=\(myLatitude),\(myLongitude)&radius=\(mDistance)&types=\(queryTyper)&sensor=true&key=\(QUERY_GOOGLE_PLACES_API_KEY)")
        
        
        print("https://maps.googleapis.com/maps/api/place/search/json?location=\(myLatitude),\(myLongitude)&radius=\(mDistance)&types=\(queryTyper)&sensor=true&key=\(QUERY_GOOGLE_PLACES_API_KEY)")
        var urlData: Data?
        do {
            try urlData = Data(contentsOf: SERVER_URL!)
        } catch {
            
        }
        var jsonData = JSON(data: urlData!)
        
        let status = jsonData["status"].stringValue
        let ok = "OK"
        var results = jsonData["results"].arrayValue
        
        print(jsonData)
        
        if status == ok {
            
            for i in 0...results.count {
                
                let name = results[i]["name"].stringValue
                //var geometry = results[i]["geometry"].arrayValue
                
                switch name {
                    
                    case "Family Mart":
                        
                        
                        mFamilyMart.append(name)
                        let lat = results[i]["geometry"]["location"]["lat"].doubleValue
                        let lng = results[i]["geometry"]["location"]["lng"].doubleValue
                        mFamilyMartLat.append(lat)
                        mFamilyMartLng.append(lng)
                    
                    case "7-ELEVEN":
                        
                        mSevenEleven.insert(name, at: 0)
                        let lat = results[i]["geometry"]["location"]["lat"].doubleValue
                        let lng = results[i]["geometry"]["location"]["lng"].doubleValue
                        mSevenElevenLat.append(lat)
                        mSevenElevenLng.append(lng)
                    
                    case "7-Eleven":
                        
                        mSevenEleven.append(name)
                        let lat = results[i]["geometry"]["location"]["lat"].doubleValue
                        let lng = results[i]["geometry"]["location"]["lng"].doubleValue
                        mSevenElevenLat.append(lat)
                        mSevenElevenLng.append(lng)
                    
                    case "Hilife":
                        
                        mHilife.append(name)
                        let lat = results[i]["geometry"]["location"]["lat"].doubleValue
                        let lng = results[i]["geometry"]["location"]["lng"].doubleValue
                        mHilifeLat.append(lat)
                        mHilifeLng.append(lng)
                    
                    case "OK Mart":
                        
                        mOKMart.append(name)
                        let lat = results[i]["geometry"]["location"]["lat"].doubleValue
                        let lng = results[i]["geometry"]["location"]["lng"].doubleValue
                        mOKMartLat.append(lat)
                        mOKMartLng.append(lng)
                    
                    
                    default:
                        break
                }
            }
            
            
        }
    }
}

extension UIImage {
    
    class func animatedImageWithData(_ data: Data) -> UIImage? {
        let source = CGImageSourceCreateWithData(data as CFData, nil)
        let image = UIImage.animatedImageWithSource(source!)
        return image
    }
    
    class func delayForImageAtIndex(_ index: Int, source: CGImageSource!) -> Double {
        
        var delay = 0.1
        
        // Get dictionaries
        let cfProperties = CGImageSourceCopyPropertiesAtIndex(source, index, nil)
        let gifProperties: CFDictionary = unsafeBitCast(
            CFDictionaryGetValue(cfProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFDictionary).toOpaque()),
            to: CFDictionary.self)
        
        // Get delay time
        var delayObject: AnyObject = unsafeBitCast(
            CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFUnclampedDelayTime).toOpaque()),
            to: AnyObject.self)
        if delayObject.doubleValue == 0 {
            delayObject = unsafeBitCast(CFDictionaryGetValue(gifProperties,
                Unmanaged.passUnretained(kCGImagePropertyGIFDelayTime).toOpaque()), to: AnyObject.self)
        }
        
        delay = delayObject as! Double
        
        if delay < 0.1 {
            delay = 0.1 // Make sure they're not too fast
        }
        
        
        return delay
    }
    
    class func gcdForPair(_ a: Int?, _ b: Int?) -> Int {
        var a = a, b = b
        // Check if one of them is nil
        if b == nil || a == nil {
            if b != nil {
                return b!
            } else if a != nil {
                return a!
            } else {
                return 0
            }
        }
        
        // Swap for modulo
        if a < b {
            let c = a
            a = b
            b = c
        }
        
        // Get greatest common divisor
        var rest: Int
        while true {
            rest = a! % b!
            
            if rest == 0 {
                return b! // Found it
            } else {
                a = b
                b = rest
            }
        }
    }
    
    class func gcdForArray(_ array: Array<Int>) -> Int {
        if array.isEmpty {
            return 1
        }
        
        var gcd = array[0]
        
        for val in array {
            gcd = UIImage.gcdForPair(val, gcd)
        }
        
        return gcd
    }
    
    class func animatedImageWithSource(_ source: CGImageSource) -> UIImage? {
        let count = CGImageSourceGetCount(source)
        var images = [CGImage]()
        var delays = [Int]()
        
        // Fill arrays
        for i in 0..<count {
            // Add image
            images.append(CGImageSourceCreateImageAtIndex(source, i, nil)!)
            
            // At it's delay in cs
            let delaySeconds = UIImage.delayForImageAtIndex(Int(i),
                source: source)
            delays.append(Int(delaySeconds * 1000.0)) // Seconds to ms
        }
        
        // Calculate full duration
        let duration: Int = {
            var sum = 0
            
            for val: Int in delays {
                sum += val
            }
            
            return sum
            }()
        
        // Get frames
        let gcd = gcdForArray(delays)
        var frames = [UIImage]()
        
        var frame: UIImage
        var frameCount: Int
        for i in 0..<count {
            frame = UIImage(cgImage: images[Int(i)])
            frameCount = Int(delays[Int(i)] / gcd)
            
            for _ in 0..<frameCount {
                frames.append(frame)
            }
        }
        
        // Heyhey
        let animation = UIImage.animatedImage(with: frames,
            duration: Double(duration) / 1000.0)
        
        return animation
    }
    
}
