//
//  MapViewController.swift
//  Around-U
//
//  Created by haochanglee on 2015/5/9.
//  Copyright (c) 2015年 Ace.Lee. All rights reserved.
//

import UIKit
import GoogleMaps
import SwiftyJSON


class GMSMapViewController: UIViewController, GMSMapViewDelegate, CLLocationManagerDelegate {
    
    var mStatusBar = UIView()
    var mGMSMapView = GMSMapView()
    var mBottomBar = UIView()
    var mBottomBackButton = UIButton()
    var mMapMark = GMSMarker()
    var mLocationManger = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        mLocationManger.delegate = self
        
        mStatusBar.translatesAutoresizingMaskIntoConstraints = false
        mStatusBar.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.view.addSubview(mStatusBar)
        
        let mStatusBarCons = [
            mSetConstraint.ConstraintTop(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintLeading(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintHeight(mStatusBar, constant: 20)
        ]
        
        self.view.addConstraints(mStatusBarCons)
        
        mGMSMapView.translatesAutoresizingMaskIntoConstraints = false
        mGMSMapView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.view.addSubview(mGMSMapView)
        
        let mGMSMapViewCons: [NSLayoutConstraint]  = [
            mSetConstraint.ConstraintTop(mGMSMapView, toItem: mStatusBar, constant: 70),
            mSetConstraint.ConstraintBottom(mGMSMapView, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintLeading(mGMSMapView, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mGMSMapView, toItem: self.view, constant: 0)
            
        ]
        
        self.view.addConstraints(mGMSMapViewCons)
        
        mBottomBar.translatesAutoresizingMaskIntoConstraints = false
        mBottomBar.backgroundColor = UIColor.clear
        self.view.addSubview(mBottomBar)
        
        let mBottomBarCons: [NSLayoutConstraint] = [
            mSetConstraint.ConstraintBottom(mBottomBar, toItem: mGMSMapView, constant: 0),
            mSetConstraint.ConstraintLeading(mBottomBar, toItem: mGMSMapView, constant: 0),
            mSetConstraint.ConstraintTrailing(mBottomBar, toItem: mGMSMapView, constant: 0),
            mSetConstraint.ConstraintHeight(mBottomBar, constant: 49)
        ]
        
        self.view.addConstraints(mBottomBarCons)
        
        mBottomBackButton.translatesAutoresizingMaskIntoConstraints = false
        mBottomBackButton.setImage(UIImage(named: "icon_back@3x")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        mBottomBackButton.imageView?.tintColor = UIColor.black.withAlphaComponent(0.8)
        mBottomBackButton.addTarget(self, action: #selector(GMSMapViewController.backButton(_:)), for: UIControlEvents.touchUpInside)
        mBottomBar.addSubview(mBottomBackButton)
        
        let mBottomBackButtonCons: [NSLayoutConstraint] = [
            mSetConstraint.ConstraintCenterX(mBottomBackButton, toItem: mBottomBar, constant: 0.0),
            mSetConstraint.ConstraintCenterY(mBottomBackButton, toItem: mBottomBar, constant: 0.0),
            mSetConstraint.ConstraintHeight(mBottomBackButton, constant: 25),
            mSetConstraint.ConstraintWidth(mBottomBackButton, constant: 25)
        ]
        
        mBottomBar.addConstraints(mBottomBackButtonCons)
        
        
        // Do any additional setup after loading the view.
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        for i in 0 ..< mNewMarkCount {
            let position = CLLocationCoordinate2DMake(mNewMarkLat[i], mNewMarkLng[i])
            let marker = GMSMarker(position: position)
            marker.title = mNewMarkName
            marker.map = mGMSMapView
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == CLAuthorizationStatus.authorizedAlways {
            
            manager.startUpdatingLocation()
            
            let myCameraPosition = GMSCameraPosition.camera(withLatitude: myLatitude, longitude: myLongitude, zoom: 16)
            //var visibleRegion: GMSVisibleRegion
            
            mMapMark.position = myCameraPosition.target
            mMapMark.snippet = "Hello World"
            mMapMark.appearAnimation = kGMSMarkerAnimationPop
            mMapMark.map = mGMSMapView
            mGMSMapView.camera = myCameraPosition
            
            mGMSMapView.animate(to: myCameraPosition)
            mGMSMapView.animate(toZoom: 16)
            mGMSMapView.isMyLocationEnabled = true
            mGMSMapView.settings.myLocationButton = true
            mGMSMapView.settings.compassButton = true
            mGMSMapView.delegate = self
            //mapView(self.mapView, willMove: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func backButton(_ sender: UIButton) {
        if self.navigationController != nil {
            self.navigationController!.popViewController(animated: true)
        }
        mGMSMapView.clear()
        //self.dismissViewControllerAnimated(true, completion: nil)
        print("你按到了")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
