//
//  ViewController.swift
//  Around-U
//
//  Created by haochanglee on 2015/5/9.
//  Copyright (c) 2015年 Ace.Lee. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    var mStatusBar = UIView()
    var mView = UIView()
    var mFirstTitle = UILabel()
    var mTouchMe = UILabel()
    var mCenterButton = UIButton()
    var mLocationManager = CLLocationManager()
    var distance = ["500", "1000", "1500", "2000"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        //mLocationManager.requestWhenInUseAuthorization()
        mLocationManager.delegate = self
        
        mStatusBar.translatesAutoresizingMaskIntoConstraints = false
        mStatusBar.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.view.addSubview(mStatusBar)
        
        let mStatusBarCons: [NSLayoutConstraint] = [
            mSetConstraint.ConstraintTop(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintLeading(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintHeight(mStatusBar, constant: 20)
        ]
        self.view.addConstraints(mStatusBarCons)
        
        /*
        mADBannerView.translatesAutoresizingMaskIntoConstraints = false
        mADBannerView.delegate = self
        self.view.addSubview(mADBannerView)
        
        mSetConstraint.Constraints = [
            mSetConstraint.ConstraintTop(mADBannerView, toItem: mStatusBar, constant: 20),
            mSetConstraint.ConstraintLeading(mADBannerView, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mADBannerView, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintHeight(mADBannerView, constant: 50)
        
        ]
        
        self.view.addConstraints(mSetConstraint.Constraints)
        */
        
        mView.translatesAutoresizingMaskIntoConstraints = false
        mView.backgroundColor = UIColor.white
        self.view.addSubview(mView)
        let mViewCons: [NSLayoutConstraint] = [
            mSetConstraint.ConstraintTop(mView, toItem: mStatusBar, constant: 70),
            mSetConstraint.ConstraintBottom(mView, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintLeading(mView, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mView, toItem: self.view, constant: 0)
        ]
        self.view.addConstraints(mViewCons)
        
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "center_button", withExtension: "gif")!)
        let centerButton = UIImage.animatedImageWithData(imageData!)?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        
        mCenterButton.translatesAutoresizingMaskIntoConstraints = false
        mCenterButton.setImage(centerButton, for: .normal)
        mCenterButton.imageView?.tintColor = UIColor.black.withAlphaComponent(0.8)
        mCenterButton.addTarget(self, action: #selector(ViewController.centerButton(_:)), for: UIControlEvents.touchUpInside)
        mView.addSubview(mCenterButton)
        
        let mCenterButtonCons: [NSLayoutConstraint] = [
            mSetConstraint.ConstraintCenterX(mCenterButton, toItem: mView, constant: 0.0),
            mSetConstraint.ConstraintCenterY(mCenterButton, toItem: mView, constant: 0.0),
            mSetConstraint.ConstraintHeight(mCenterButton, constant: 50),
            mSetConstraint.ConstraintWidth(mCenterButton, constant: 50)
        ]
        
        mView.addConstraints(mCenterButtonCons)
        
        mFirstTitle.translatesAutoresizingMaskIntoConstraints = false
        mFirstTitle.textAlignment = NSTextAlignment.center
        mFirstTitle.numberOfLines = 0
        mFirstTitle.text = "AROUND U"
        mFirstTitle.font = UIFont.boldSystemFont(ofSize: 24)
        mView.addSubview(mFirstTitle)
        
        let mFirstTitleCons: [NSLayoutConstraint] = [
            mSetConstraint.ConstraintCenterX(mFirstTitle, toItem: mView, constant: 0.0),
            mSetConstraint.ConstraintCenterY(mFirstTitle, toItem: mView, constant: -65.0),
            mSetConstraint.ConstraintHeight(mFirstTitle, constant: 100),
            mSetConstraint.ConstraintWidth(mFirstTitle, constant: 110)
        ]
        
        mView.addConstraints(mFirstTitleCons)
        
        mTouchMe.translatesAutoresizingMaskIntoConstraints = false
        mTouchMe.textAlignment = NSTextAlignment.center
        mTouchMe.numberOfLines = 0
        mTouchMe.text = "Touch Me"
        mTouchMe.font = UIFont.boldSystemFont(ofSize: 12)
        mView.addSubview(mTouchMe)
        
        mSetConstraint.Constraints = [
            mSetConstraint.ConstraintCenterX(mTouchMe, toItem: mView, constant: 0.0),
            mSetConstraint.ConstraintCenterY(mTouchMe, toItem: mView, constant: 45.0),
            mSetConstraint.ConstraintHeight(mTouchMe, constant: 100),
            mSetConstraint.ConstraintWidth(mTouchMe, constant: 50)
            
        ]
        
        mView.addConstraints(mSetConstraint.Constraints as! [NSLayoutConstraint])
        
        let ButtonView = distanceButtonView()
        
        let distanceButton = DWBubbleMenuButton(frame: CGRect(x: UIScreen.main.bounds.width / 2 - 25,
            y: UIScreen.main.bounds.height / 2 + 40,
            width: ButtonView.frame.size.width,
            height: ButtonView.frame.size.height), expansionDirection: ExpansionDirection.directionDown)

        
        distanceButton.translatesAutoresizingMaskIntoConstraints = false
        distanceButton.homeButtonView = ButtonView
        distanceButton.addButtons(self.createDemoButtonArray())
        
        mView.addSubview(distanceButton)
        
        /*
        mSetConstraint.Constraints = [
            mSetConstraint.ConstraintCenterX(distanceButton, toItem: mView, constant: 0.0),
            mSetConstraint.ConstraintCenterY(distanceButton, toItem: mView, constant: 105.0),
            mSetConstraint.ConstraintHeight(distanceButton, constant: 50),
            mSetConstraint.ConstraintWidth(distanceButton, constant: 50)
        ]
        
        mView.addConstraints(mSetConstraint.Constraints)
        */
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == CLAuthorizationStatus.authorizedAlways {
            
            self.mLocationManager.startUpdatingLocation()
            
            myLatitude = (self.mLocationManager.location?.coordinate.latitude)!
            myLongitude = (self.mLocationManager.location?.coordinate.longitude)!
            
            print(myLatitude)
            print(myLongitude)
        }
    }
    
    
    /*
    func distanceButtonView() -> UILabel {
        
        var label = UILabel(frame: CGRectMake(0.0, 0.0, 40.0, 40.0))
        
        label.text = "Tap";
        label.textColor = UIColor.whiteColor()
        label.textAlignment = NSTextAlignment.Center
        label.layer.cornerRadius = label.frame.size.height / 2.0;
        label.backgroundColor = UIColor(red:0.0,green:0.0,blue:0.0,alpha:0.5)
        label.clipsToBounds = true;
        
        return label;
    }
    */
    
    func distanceButtonView() -> UIImageView {
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        imageView.image = UIImage(named: "setting@3x")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        
        imageView.tintColor = UIColor.black.withAlphaComponent(0.8)
        
        return imageView
    }

    func createDemoButtonArray() -> [UIButton] {
        var buttons:[UIButton]=[]
        for s in 0...distance.count {
            let button:UIButton = UIButton(type: .system)
            button.setTitleColor(UIColor.black, for: UIControlState())
            button.setTitle("\(s)m", for: UIControlState())
            
            button.frame = CGRect(x: 0.0, y: 0.0, width: 80.0, height: 30.0)
            //button.layer.cornerRadius = button.frame.size.height / 2.0
            button.backgroundColor = UIColor.clear
            button.clipsToBounds = true
            button.tag = s
            
            
            button.addTarget(self, action: #selector(ViewController.buttonTap(_:)), for: UIControlEvents.touchUpInside)
            
            buttons.append(button)
            
        }
        return buttons
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    func buttonTap(_ sender:UIButton){
        
        mDistance = distance[sender.tag]
        print("\(mDistance)")
        print("Button tapped, tag:\(sender.tag)")
    }
    
    func centerButton(_ sender: UIButton) {
        
        let _vc = ClassificationViewController()
        mQuery.googlePlaces("convenience_store" as AnyObject)
        self.navigationController?.pushViewController(_vc, animated: true)
        //self.showDetailViewController(_vc, sender: self)
        
        
        print("你有按到我")
    }


}

