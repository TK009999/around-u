//
//  ClassificationViewController.swift
//  Around-U
//
//  Created by haochanglee on 2015/5/9.
//  Copyright (c) 2015年 Ace.Lee. All rights reserved.
//

import UIKit
import iAd
import GoogleMaps
import SwiftyJSON

class ClassificationViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    var mStatusBar = UIView()
    var mBottomBar = UIView()
    var mBottomBackButton = UIButton()
    var mSearchBar = UISearchBar()
    var mCollectionView: UICollectionView!
    var mLayout = UICollectionViewFlowLayout()
    var mCell = UICollectionViewCell.self
    
    var mItemImages = ["7-11", "FAMILY-MART", "HI-LIFE", "OK", "M",  "police", "WATSONS", "WV", "STARBUCKS", "matsusei", "eslite", "wellcome", "carrefour", "cosmed", "UNIQLO", "tw", "SUBWAY", "POYA", "muji", "MOS", "mart", "daiso", "decor-house", "KFC", "hilidayktv"]
    var mItemNames = ["7-ELEVEN", "全家便利商店", "萊爾富", "OK便利商店", "麥當勞", "警察局", "屈臣氏", "全聯", "星巴克", "松青超市", "誠品書店", "頂好", "家樂福", "康是美", "UNIQLO", "台灣彩卷", "SUBWAY", "寶雅", "無印良品", "摩斯漢堡", "美聯社", "大創39元百貨", "特力家居", "肯德基", "好樂迪KTV"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        //self.modalTransitionStyle = UIModalTransitionStyle.CrossDissolve
        self.view.backgroundColor = UIColor.white
        mStatusBar.translatesAutoresizingMaskIntoConstraints = false
        mStatusBar.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.view.addSubview(mStatusBar)
        let cons = [
            mSetConstraint.ConstraintTop(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintLeading(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mStatusBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintHeight(mStatusBar, constant: 20)
        ]
        self.view.addConstraints(cons)
        
        /*
        mSearchBar.translatesAutoresizingMaskIntoConstraints = false
        mSearchBar.searchBarStyle = UISearchBarStyle.Minimal
        mSearchBar.backgroundColor = UIColor.clearColor()
        self.view.addSubview(mSearchBar)
        
        mSetConstraint.Constraints = [
            mSetConstraint.ConstraintTop(mSearchBar, toItem: mStatusBar, constant: 70),
            mSetConstraint.ConstraintLeading(mSearchBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mSearchBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintHeight(mSearchBar, constant: 44)
        ]
        
        self.view.addConstraints(mSetConstraint.Constraints)
        */
        
        mBottomBar.translatesAutoresizingMaskIntoConstraints = false
        mBottomBar.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.view.addSubview(mBottomBar)
        let bottomBarCons: [NSLayoutConstraint] = [
            mSetConstraint.ConstraintBottom(mBottomBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintLeading(mBottomBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mBottomBar, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintHeight(mBottomBar, constant: 49)
        ]
        self.view.addConstraints(bottomBarCons)
        
        mBottomBackButton.translatesAutoresizingMaskIntoConstraints = false
        mBottomBackButton.setImage(UIImage(named: "icon_back@3x")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState())
        mBottomBackButton.imageView?.tintColor = UIColor.white
        mBottomBackButton.addTarget(self, action: "backButton:", for: UIControlEvents.touchUpInside)
        mBottomBar.addSubview(mBottomBackButton)
        
        let bottomBackButtonCons: [NSLayoutConstraint] = [
            mSetConstraint.ConstraintCenterX(mBottomBackButton, toItem: mBottomBar, constant: 0.0),
            mSetConstraint.ConstraintCenterY(mBottomBackButton, toItem: mBottomBar, constant: 0.0),
            mSetConstraint.ConstraintHeight(mBottomBackButton, constant: 25),
            mSetConstraint.ConstraintWidth(mBottomBackButton, constant: 25)
        ]
        mBottomBar.addConstraints(bottomBackButtonCons)
        
        mLayout.minimumInteritemSpacing = 0.0
        mLayout.minimumLineSpacing = 5.0
        mLayout.sectionInset = UIEdgeInsetsMake(5.0, 5.0, 0.0, 5.0)
        mLayout.itemSize = CGSize(width: 100, height: 100)
        
        //mCollectionView.collectionViewLayout = mLayout
        mCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: mLayout)
        
        mCollectionView.translatesAutoresizingMaskIntoConstraints = false
        mCollectionView.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        mCollectionView.register(mCell, forCellWithReuseIdentifier: "cell")
        mCollectionView.delegate = self
        mCollectionView.dataSource = self        
        self.view.addSubview(mCollectionView)
        mSetConstraint.Constraints = [
            mSetConstraint.ConstraintTop(mCollectionView, toItem: mStatusBar, constant: 70),
            mSetConstraint.ConstraintLeading(mCollectionView, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintTrailing(mCollectionView, toItem: self.view, constant: 0),
            mSetConstraint.ConstraintBottom(mCollectionView, toItem: mBottomBar, constant: -49)
        
        ]
        self.view.addConstraints(mSetConstraint.Constraints as! [NSLayoutConstraint])

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return mLayout.itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var cell_label: UILabel!
        var cell_imageView: UIImageView!
        let mItemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) 
        mItemCell.backgroundColor = UIColor.clear
        mItemCell.layer.cornerRadius = 10
        mItemCell.layer.masksToBounds = true
        
        cell_imageView = UIImageView()
        cell_imageView.translatesAutoresizingMaskIntoConstraints = false
        cell_imageView.image = UIImage(named: "\(mItemImages[indexPath.row])@3x")
        
        mItemCell.addSubview(cell_imageView)
        
        mSetConstraint.Constraints = [
            mSetConstraint.ConstraintCenterX(cell_imageView, toItem: mItemCell, constant: 0.0),
            mSetConstraint.ConstraintCenterY(cell_imageView, toItem: mItemCell, constant: -15.0),
            mSetConstraint.ConstraintHeight(cell_imageView, constant: 50),
            mSetConstraint.ConstraintWidth(cell_imageView, constant: 50)
        ]
        
        mItemCell.addConstraints(mSetConstraint.Constraints as! [NSLayoutConstraint])
        
        cell_label = UILabel()
        cell_label.text = "\(mItemNames[indexPath.row])"
        cell_label.translatesAutoresizingMaskIntoConstraints = false
        cell_label.textColor = UIColor.white
        cell_label.font = UIFont.systemFont(ofSize: 13)
        //cell_label.backgroundColor = UIColor.redColor()
        cell_label.textAlignment = NSTextAlignment.center
        //cell_label.numberOfLines = 0
        mItemCell.addSubview(cell_label)
        
        mSetConstraint.Constraints = [
            mSetConstraint.ConstraintCenterX(cell_label, toItem: mItemCell, constant: 0.0),
            mSetConstraint.ConstraintTop(cell_label, toItem: cell_imageView, constant: 55),
            mSetConstraint.ConstraintWidth(cell_label, constant: 100),
            mSetConstraint.ConstraintHeight(cell_label, constant: 30)
        ]
        
        mItemCell.addConstraints(mSetConstraint.Constraints as! [NSLayoutConstraint])
        
        return mItemCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            mNewMarkCount = mSevenElevenLng.count
            mNewMarkLat = mSevenElevenLat
            mNewMarkLng = mSevenElevenLng
            mNewMarkName = mItemNames[0]
            var _vc = GMSMapViewController()
            self.navigationController?.pushViewController(_vc, animated: true)
        }
        
        if indexPath.row == 1 {
            mNewMarkCount = mFamilyMart.count
            mNewMarkLat = mFamilyMartLat
            mNewMarkLng = mFamilyMartLng
            mNewMarkName = mItemNames[1]
            let _vc = GMSMapViewController()
            self.navigationController?.pushViewController(_vc, animated: true)
        }
        
        if indexPath.row == 2 {
            mNewMarkCount = mHilife.count
            mNewMarkLat = mHilifeLat
            mNewMarkLng = mHilifeLng
            mNewMarkName = mItemNames[2]
            var _vc = GMSMapViewController()
            self.navigationController?.pushViewController(_vc, animated: true)
        }
        
        if indexPath.row == 3 {
            mNewMarkCount = mOKMart.count
            mNewMarkLat = mOKMartLat
            mNewMarkLng = mOKMartLng
            mNewMarkName = mItemNames[3]
            var _vc = GMSMapViewController()
            self.navigationController?.pushViewController(_vc, animated: true)
        }
        
        /*
        for var i = 0; i < 6; i++ {
            
            
            if indexPath.row == i {
                
                mQuery.googlePlaces(queryTyper: "convenience_store")
                
                var _vc = GMSMapViewController()
                self.navigationController?.pushViewController(_vc, animated: true)
                print("\(i)")
            }
        }
        */
    }
    
    func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        //self.dismissViewControllerAnimated(true, completion: nil)
        print("你按到了")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
